<!DOCTYPE html>
<html lang="en">
<?php include("header.php");?>
<section id="banner">
	<div class="container">
		<div class="navbar-header"> 
			<div id="call" class="visible-xs"><i class="glyphicon glyphicon-earphone"></i> <span class="AVANSERnumber"><a href="tel:1300667787">1300 66 77 87</a></span></div>
           <!-- responsive nav button -->
			<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> 
				<span class="sr-only">Toggle navigation</span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
			</button>
            <!-- /responsive nav button --> 
    
				<!-- logo -->
				<h1 class="navbar-brand"> <a href="/daniels/">Daniels <span>Making Healthcare Safer</span></a></h1>
				<!-- /logo --> 
		</div>
		   <!-- main nav -->
			<nav class="collapse navbar-collapse navbar-right" role="navigation">
				<div id="call" class="hidden-xs"><i class="glyphicon glyphicon-earphone"></i> <span class="AVANSERnumber"><a href="tel:1300667787">1300 66 77 87</a></span></div>
					<div id="menu">
						<ul class="nav navbar-nav">
						<li><a href="/daniels/#whoweservice">Who We Service</a></li>
						<li><a href="/daniels/#whychooseus">Why Choose Us</a></li>
						<li><a href="/daniels/#ourapproach">Our Approach</a></li>
						<li><a href="/daniels/#ourproducts">Our Products</a></li>
						<li><a href="/daniels/#ourexpertise">Our Expertise</a></li>
						<li><a href="/daniels/#requestquote">Free Quote</a></li>
						</ul>
					</div>
				</div>
			</nav>
		    <!-- /main nav --> 
		
	        <!--img class="right" src="http://192.168.65.133/daniels/images/logo.png" alt=""-->
		
				<div class="banner-caption">
					<h2 class="wow slideInDown">A reliable and simple service solution <br>for clinical waste and sharps disposal</h2>
					<a class="btn btn-default wow zoomIn" href="#requestquote" data-wow-delay=".3s">Request your FREE Consultation &amp; Quote</a>
					<p class="wow zoomIn"><i class="glyphicon glyphicon-earphone"></i>  <span class="AVANSERnumber"><a href="tel:1300667787">1300 66 77 87</a></span></p>
				</div>
		</div>
	</div>
	
	
</section>

<section id="slogan">
<div class="container">
<h3 class="text-center wow ">Daniels Health. The industry leader in medical waste management</h3>
</div>
</section>
<!-- SLOGAN [END] -->


<section id="whoweservice">
<div class="container">
<h2 class="wow ">Who We Service</h2>
<ul class="whoweservice-list">
    <li class="wow " data-wow-delay=".5s">
    <img alt="" width="140" src="/daniels/images/icon-gp-clinics.png" />
    <h4>GP Clinics</h4>
    </li>
    <li class="wow " data-wow-delay=".6s">
    <img alt="" width="140" src="/daniels/images/icon-vets.png" />
    <h4>Vets</h4>
    </li>
    <li class="wow " data-wow-delay=".7s">
    <img alt="" width="140" src="/daniels/images/icon-dentists.png" />
    <h4>Dentists</h4>
    </li>
    <li class="wow " data-wow-delay=".8s">
    <img alt="" width="140" src="/daniels/images/icon-surgical-centres.png" />
    <h4>Surgical Centres</h4>
    </li>
    <li class="wow " data-wow-delay=".9s">
    <img alt="" width="140" src="/daniels/images/icon-nursing-homes.png" />
    <h4>Nursing Homes</h4>
    </li>
</ul>
<br />
<h3 class="pickup wow ">Pick up Schedule Customised to your waste &amp; volume requirements</h3>
</div>
</section>



<!-- WHO WE SERVICE [END] -->
<section id="whychooseus">
<div class="container">
<h2 class="wow ">Why Choose Us</h2>
<br />
<div class="row">
<div class="col-md-5 col-sm-5 wow ">
<div class="imgbox"><img alt="" src="/daniels/images/img-why-choose-us.jpg" />
</div>
</div>
<div class="col-md-7 col-sm-7 wow ">
<ol class="custom">
    <li>
    <h6>Simple approach</h6>
    <p>We make our service simple &amp; easy to use</p>
    </li>
    <li>
    <h6>Reliable Service</h6>
    <p>We service on time every time</p>
    </li>
    <li>
    <h6>Flexible</h6>
    <p>We can adjust your service according to your needs</p>
    </li>
    <li>
    <h6>Leading products</h6>
    <p>We design &amp; engineer our own products specially for the medical industry</p>
    </li>
    <li>
    <h6>Safety</h6>
    <p>Our unparalleled standards in quality and hygiene ensure your staff and patients are protected - we make your safety our priority</p>
    </li>
    <li>
    <h6>Experience</h6>
    <p>With 30 years experience in the industry, we are experts in compliance. We are EPA licensed, and certified with ISO-9001 Quality
    Management &amp; Safety 18001</p>
    </li>
    <li>
    <h6>Australian owned</h6>
    <p>We are an Australian owned family business employing over 300 people across the country</p>
    </li>
</ol>
</div>
</div>
</div>
</section>



<!-- WHY CHOOSE US [END] -->
<section id="ourapproach">
<div class="container">
<h2 class="wow ">Our Approach</h2>
<br />
<p class="wow ">We love what we do and take pride in our service.
<br />
From our friendly drivers to our professional customer service team,
<br />
we respond to your needs ensuring your waste is managed reliably and efficiently</p>
<br />
<h5 class="wow ">Leading best practice in</h5>
<ul class="approach-list wow " data-wow-delay=".5s">
    <li><span class="btn btn-default btn-lg">Sharps safety</span>
    </li>
    <li><span class="btn btn-default btn-lg">Infection control</span>
    </li>
    <li><span class="btn btn-default btn-lg">Cleanliness</span>
    </li>
    <li><span class="btn btn-default btn-lg">Compliance</span>
    </li>
    <li><span class="btn btn-default btn-lg">Waste segregation</span>
    </li>
    <li><span class="btn btn-default btn-lg">User Efficiency</span>
    </li>
</ul>
<br />
<h4 class="wow " data-wow-delay=".4s">5 Simple steps to getting started</h4>
<br />
<p class="wow " data-wow-delay=".4s"><img alt="" width="495" src="/daniels/images/img-5simple-steps.jpg" />
</p>
</div>
</section>
<!-- OUR APPROACH [END] -->
<section id="ourproducts">
<div class="container">
<h2 class="wow ">Our Products</h2>
<p class="wow ">We have specialised products designed and engineered for the medical industry</p>
<div class="product-slider">
<div class="slide-item wow " data-wow-delay=".6s">
<div class="product-box">
<img alt="" src="/daniels/images/img-product-1.jpg" />
<h6 class="btn btn-default fluid">Reusable Sharps</h6>
</div>
</div>
<div class="slide-item wow " data-wow-delay=".5s">
<div class="product-box">
<img alt="" src="/daniels/images/img-product-2.jpg" />
<h6 class="btn btn-default fluid">Disposable sharps</h6>
</div>
</div>
<div class="slide-item wow " data-wow-delay=".4s">
<div class="product-box">
<img alt="" src="/daniels/images/img-product-3.jpg" />
<h6 class="btn btn-default fluid">Clinical waste</h6>
</div>
</div>
<div class="slide-item wow " data-wow-delay=".3s">
<div class="product-box">
<img alt="" src="/daniels/images/img-product-4.jpg" />
<h6 class="btn btn-default fluid">Sanitary waste</h6>
</div>
</div>
<div class="slide-item wow " data-wow-delay=".2s">
<div class="product-box">
<img alt="" src="/daniels/images/img-product-5.jpg" />
<h6 class="btn btn-default fluid">Security waste</h6>
</div>
</div>
</div>
</div>
</section>
<!-- OUR PRODUCT [END] -->
<section id="safetysolutions">
<div class="container">
<h2 class="wow ">Safety Engineered Solutions</h2>
<p class="wow ">The revolutionary Daniels Sharpsmart and Clinismart systems are
<br />
scientifically proven to reduce injury and infection risk</p>
<br />
<br />
<div class="row">
<div class="col-md-6 solution-box left  wow " data-wow-delay=".3s">
<div class="solution-img right"><img alt="" width="225" src="/daniels/images/img-sharpsmart.png" />
</div>
<div class="solution-desc left">
<ul>
    <li>Engineered Sharps Collector</li>
    <li>Wide opening for optimal acess</li>
    <li>Safety tray that protects contents &amp; eliminates overfilling</li>
    <li>Clearview window tocheck contents level</li>
    <li>Impenetrable medical grade hardened plastic</li>
    <li>Leak-proof seal</li>
    <li>Tamper-proof side locks to secure collector when full</li>
</ul>
</div>
</div>
<div class="col-md-6 solution-box left  wow " data-wow-delay=".1s">
<div class="solution-img left"><img alt="" width="240" src="/daniels/images/img-clinismart.png" />
</div>
<div class="solution-desc right">
<ul>
    <li>Engineered Medical Waste Collector</li>
    <li>Aesthetically designed for point-of-use placement</li>
    <li>Silicon seal for odour containment</li>
    <li>Temporary front locks for added security</li>
    <li>Tamper-proof side locks to secure collector when full</li>
    <li>Small Footprint</li>
    <li>Rear insert for mounting on foot pedal operated accessories</li>
</ul>
</div>
</div>
</div>
</div>
</section>
<!-- Safety Solutions [END] -->
<section id="ourexpertise">
<div class="container">
<h2 class="wow ">our expertise</h2>
<br />
<ul class="expertise-list">
    <li class="wow ">
    <h3>30</h3>
    <p>Years
    <br />
    Experience</p>
    </li>
    <li class="wow ">
    <h3>10,000</h3>
    <p>Customers
    <br />
    Partnering with us</p>
    </li>
    <li class="wow ">
    <h3>79</h3>
    <p>Vehicles in
    <br />
    our Service Fleet</p>
    </li>
    <li class="wow ">
    <h3>225,000</h3>
    <p>services
    <br />
    fulfilled annually</p>
    </li>
</ul>
</div>
</section>
<!-- Safety Solutions [END] -->
<section id="requestquote">
<div class="container">
<h2 class="wow ">Request your FREE Consultation & Quote<br>
    <i class="glyphicon glyphicon-earphone"></i>  <a href="tel:1300 66 77 87">1300 66 77 87</a></h2>
<br>
<!--<form name="catwebformform82643" method="post" onsubmit="return checkWholeForm82643 (this)" enctype="multipart/form-data" action="/FormProcessv2.aspx?WebFormID=113706&amp;OID=16704063&amp;OTYPE=1&amp;EID=0&amp;CID=0">-->
<form name="catwebformform82643" method="post" onsubmit="return checkWholeForm82643 (this)" enctype="multipart/form-data" action="mail.php">
    <div class="row">
        <div class="col-md-4 col-sm-6 field wow ">
            <label for="">First Name</label>
            <input type="text" class="form-control" name="FirstName" id="FirstName" data-validation="required">
        </div>
        <div class="col-md-4 col-sm-6 field wow ">
            <label for="">Last Name</label>
            <input type="text" class="form-control" name="LastName" id="LastName" data-validation="required">
        </div>
        <div class="col-md-4 col-sm-6 field wow ">
            <label for="">Business Name</label>
            <input type="text" class="form-control" name="CAT_Custom_20079671" id="CAT_Custom_20079671">
        </div>
        <div class="col-md-4 col-sm-6 field wow ">
            <label for="">Email</label>
            <input type="text" class="form-control" name="EmailAddress" id="EmailAddress" data-validation="email">
        </div>
        <div class="col-md-4 col-sm-6 field wow ">
            <label for="">Phone</label>
            <input type="text" class="form-control" name="CellPhone" id="CellPhone">
        </div>

                <div class="col-md-4 col-sm-6 field wow ">
            <label for="">Post Code</label>
            <input type="text" class="form-control" name="CAT_Custom_20078916" id="CAT_Custom_20078916">
        </div>

        <div class="col-md-12 col-sm-12 wow ">
            <label for="">I am interested in...</label>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="field-check">
                        <input name="CAT_Custom_20032538" id="CAT_Custom_20032538_0" value="MEDICAL/CLINICAL WASTE" type="checkbox" class="custom">
                        <label for="CAT_Custom_20032538_0">MEDICAL/CLINICAL
                            <br>WASTE</label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="field-check">
                        <input name="CAT_Custom_20032538" id="CAT_Custom_20032538_1" value="SHARPS COLLECTORS" type="checkbox" class="custom">
                        <label for="CAT_Custom_20032538_1">Sharps
                            <br>collectors</label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="field-check">
                        <input name="CAT_Custom_20032538" id="CAT_Custom_20032538_2" value="WASTE BAGS" type="checkbox" class="custom">
                        <label for="CAT_Custom_20032538_2">Waste
                            <br>bags</label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="field-check">
                        <input name="CAT_Custom_20032538" id="CAT_Custom_20032538_3" value="SANITARY WASTE" type="checkbox" class="custom">
                        <label for="CAT_Custom_20032538_3">Sanitary
                            <br>WASTE</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="field-check">
                        <input name="CAT_Custom_20032538" id="CAT_Custom_20032538_4" value="SECURE DOCUMENT DESTRUCTION" type="checkbox" class="custom">
                        <label for="CAT_Custom_20032538_4">Secure document
                            <br>destruction</label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="field-check">
                        <input name="CAT_Custom_20032538" id="CAT_Custom_20032538_5" value="SPILL KITS" type="checkbox" class="custom">
                        <label for="CAT_Custom_20032538_5">Spill
                            <br>kits</label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="field-check">
                        <input name="CAT_Custom_20032538" id="CAT_Custom_20032538_6" value="SAFETY AND COMPLIANCE" type="checkbox" class="custom">
                        <label for="CAT_Custom_20032538_6">Safety and
                            <br>compliance</label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="field-check">
                        <input name="CAT_Custom_20032538" id="CAT_Custom_20032538_7" value="TRAINING &amp; EDUCATION" type="checkbox" class="custom">
                        <label for="CAT_Custom_20032538_7">Training &amp;
                            <br>education</label>
                    </div>
                </div>
            </div>
        </div>
		<div class="col-md-12 text-center wow clearfix">
           <!--label>Prove you're not a robot <span class="req">*</span></label-->
           <!--
            <script type="text/javascript" src="/CatalystScripts/ValidationFunctions.js?vs=b261.r522377-phase1"></script>
<script>function reCaptchaV2OnLoad() { reCaptchaV2Manager.onLoadHandler(); }</script>
<script>document.write('<scr' + 'ipt type="text/javascript" src="https://www.google.com/recaptcha/api.js?onload=reCaptchaV2OnLoad&render=explicit" async defer></scri' + 'pt>');</script>
<script>
    reCaptchaV2Manager.registerInstance({
        'id': '85d70a0c3ffc4ef0a5c6734b665658c7',
        'sitekey': '6LcGXxsUAAAAALhWpOesJevjsD2KnvmZbTZADnIJ',
        'type': 'image',
        'theme': 'white',
        'size': 'normal'
    });
</script>
<input id="token85d70a0c3ffc4ef0a5c6734b665658c7" type="hidden" data-recaptcha-id="85d70a0c3ffc4ef0a5c6734b665658c7" name="bc-recaptcha-token" value=""/>
<div id="recaptcha85d70a0c3ffc4ef0a5c6734b665658c7" class="g-recaptcha"></div>
-->
        <!--<div class="g-recaptcha" data-sitekey="6Ld4XI4UAAAAACwtO1aQXUelcDiywRTW5E-93Cot"></div>-->
        </div>
        <div class="col-md-12 action text-center wow ">
            <input type="submit" class="btn btn-default btn-lg" value="Request your FREE Consultation &amp; Quote">
        </div>
    </div>

    <!-- PAR VALIDATION -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script>

            $.validate({
                            //modules : 'location, date, security, file',
                            //onModulesLoaded : function() {
                                                           // $('#country').suggestCountry();
                            //                            }
                            modules : 'toggleDisabled',
                            disabledFormFilter : 'form.toggle-disabled',
                            showErrorDialogs : false
                        });

  // Restrict presentation length
  $('#presentation').restrictLength( $('#pres-max-length') );

</script>
    <!-- PAR VALIDATION -->
    <script type="text/javascript" src="/CatalystScripts/ValidationFunctions.js?vs=b86.r512773-phase1"></script>
   <script type="text/javascript">
//<![CDATA[
var submitcount82643 = 0;function checkWholeForm82643(theForm){var why = "";if (theForm.FirstName) why += isEmpty(theForm.FirstName.value, "First Name"); if (theForm.LastName) why += isEmpty(theForm.LastName.value, "Last Name"); if (theForm.Company) why += isEmpty(theForm.Company.value, "Company Name"); if (theForm.EmailAddress) why += checkEmail(theForm.EmailAddress.value); if (theForm.CellPhone) why += isEmpty(theForm.CellPhone.value, "Cell Phone Number"); if (theForm['g-recaptcha-response']) why += reCaptchaV2IsInvalid(theForm, "Please prove you're not a robot");if (theForm.CAT_Custom_20078916) why += isEmpty(theForm.CAT_Custom_20078916.value, "PostCode");if (theForm.CAT_Custom_20079671) why += isEmpty(theForm.CAT_Custom_20079671.value, "Business Name");if(why != ""){alert(why);return false;}if(submitcount82643 == 0){submitcount82643++;theForm.submit();return false;}else{alert("Form submission is in progress.");return false;}}
//]]>
</script></form>
</div>
</section>
<!-- REQUEST QUOTE [END] -->
        </main>
<?php include("footer.php");?>	
    
</body>
</html>