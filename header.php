<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Daniels Health</title>
    <link href="https://fonts.googleapis.com/css?family=Hind+Vadodara:400,700|Mukta:500,700" rel="stylesheet">
    <link  rel="stylesheet" href="../../../daniels/css/bootstrap.min.css">
	<link  rel="stylesheet" href="../../../daniels/css/custom.css">
	<link  rel="stylesheet" href="../../../daniels/css/slick.css">
	<link  rel="stylesheet" href="../../../daniels/stylesheets/modulestylesheets.css">
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
</head>