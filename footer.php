<footer id="footer">
<div class="container">
 <div class="footer-left">As of June 1st SteriHealth has officially rebranded to<br /> Daniels Health, returning to our founding name.<br>
<br><a href="/daniels/pdf/Daniels Health Privacy Policy.pdf" target="_blank">Privacy Policy</a>
</div>
<div class="footer-right"><img height="50" src="="http://192.168.65.133/daniels/images/new-logo-daniels.png" alt=""></div>        
<div class="clearall"></div>
</div>
</footer>
        <!-- IE9 Below Hacks -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Bootstrap, jQuery Plungins -->

<script src="/daniels/js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="/daniels/js/modernizr-2.6.2.min.js" type="text/javascript"></script>
<script src="/daniels/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/daniels/js/slick.min.js" type="text/javascript"></script>
<script src="/daniels/js/wow.min.js" type="text/javascript"></script>
<script src="/daniels/js/custom.js" type="text/javascript"></script>
        <!-- Google Code for Remarketing Tag -->
        <!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup -->
        <script type="text/javascript">
    /* <![CDATA[ */

    var google_conversion_id = 944916134;

    var google_custom_params = window.google_tag_params;

    var google_remarketing_only = true;

    /* ]]> */
</script>
        <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js"></script>
        <script type="text/javascript">
        // AVANSER Call Tracking [start] 
        (function() {
            var av = document.createElement('script');
            av.type = 'text/javascript';
            av.id = 'AVANSERjs';
            av.src = ('https:' == document.location.protocol ? 'https://' : 'http://') +
                'adriano-au.avanser.com/aa.js?t=' + (new Date().getTime());
            var h = document.getElementsByTagName('head')[0];
            h.parentNode.appendChild(av);
        })();
        // AVANSER Call Tracking [stop]
    </script>
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/944916134/?value=0&amp;guid=ON&amp;script=0" />
        </div>
    </noscript>
